package com.pany;

import com.pany.domain.Customer;
import com.pany.service.CRUDService;
import com.pany.service.CustomerServiceImpl;
import com.pany.ui.CustomerForm;
import com.pany.ui.CustomerGrid;
import com.pany.ui.TopLayout;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import javax.servlet.annotation.WebServlet;

/**
 * This UI is the application entry point. A UI may either represent a browser window
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        //layouts & components
        final CRUDService<Customer> customerService = CustomerServiceImpl.instance();
        final CustomerForm customerForm = CustomerForm.instanceOf();
        final CustomerGrid customerGrid = CustomerGrid.instanceOf(customerService, customerForm);
        final TopLayout topLayout = TopLayout.instanceOf(customerGrid, customerForm, customerService);
        //circular dependency
        customerForm.setGrid(customerGrid);
        customerForm.init();
        //look and feel

        final HorizontalLayout layout = new HorizontalLayout();
        final VerticalLayout leftLayout = new VerticalLayout();
        customerForm.setVisible(false);
        leftLayout.setSpacing(true);
        leftLayout.setMargin(true);
        layout.setMargin(true);
        layout.setSpacing(true);

        //add components

        leftLayout.addComponents(topLayout);
        leftLayout.addComponent(customerGrid);
        layout.addComponents(leftLayout, customerForm);

        setContent(layout);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
