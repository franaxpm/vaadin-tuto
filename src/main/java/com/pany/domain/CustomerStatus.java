package com.pany.domain;

import com.vaadin.data.util.converter.Converter;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author fran
 */
public enum CustomerStatus {
    NotContacted("Not Contacted"), ImportedLead("Imported Lead"), Contacted("Contacted"), Customer("Customer"), ClosedLost("Closed Lost");

    private static List<String> customerStatuses = Arrays.stream(CustomerStatus.values()).map(CustomerStatus::getCaption).collect(Collectors.toList());
    private static Map<String, CustomerStatus> customerStatusMap =  Arrays.stream(CustomerStatus.values()).collect(Collectors.toMap(CustomerStatus::getCaption,customerStatus -> customerStatus));
    private final String caption;

    CustomerStatus(String caption) {
        this.caption = caption;

    }

    public static List<String> statuses() {
        return customerStatuses;
    }

    public static Map<String, CustomerStatus> getCustomerStatusMap() {
        return customerStatusMap;
    }

    public String getCaption() {
        return caption;
    }

    public static final class CustomerStatusConverter implements Converter<CustomerStatus, String> {


        public static final Converter<CustomerStatus, String> CONVERTER = new CustomerStatusConverter();

        private CustomerStatusConverter() {
        }

        @Override
        public String convertToModel(CustomerStatus value, Class<? extends String> targetType, Locale locale) throws ConversionException {
            return value.caption;
        }

        @Override
        public CustomerStatus convertToPresentation(String value, Class<? extends CustomerStatus> targetType, Locale locale) throws ConversionException {
            return CustomerStatus.valueOf(value);
        }

        @Override
        public Class<String> getModelType() {
            return null;
        }

        @Override
        public Class<CustomerStatus> getPresentationType() {
            return null;
        }
    }


}
