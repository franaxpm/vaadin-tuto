package com.pany.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ${CLASS}
 *
 * @author fran
 */
public class Customer {

    private static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private Integer id;
    private String name;
    private String email;
    private String lastName;
    private CustomerStatus customerStatus;
    private Date birthDate;

    private Customer() {
        this.id = Integer.MIN_VALUE;
        this.name = "";
        this.email = "";
        this.lastName = "";
        this.customerStatus = CustomerStatus.NotContacted;
        this.birthDate = new Date();
    }

    private Customer(Integer id, String name, String lastName, String email, CustomerStatus customer, Date birthDate) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.lastName = lastName;
        this.customerStatus = customer;
        this.birthDate = birthDate;
    }

    /**
     * Just to avoid that the exception dirts the code... try-catch-block
     *
     * @param text
     * @return
     */
    private static synchronized Date getDate(String text) {
        try {
            return sdf.parse(text);
        } catch (ParseException e) {
            return new Date();
        }

    }

    public static Customer instanceOf(String[] line) {
        int i = 0;
        return new Customer(Integer.parseInt(line[i++]), line[i++], line[i++], line[i++], CustomerStatus.valueOf(line[i++]), getDate(line[i]));
    }

    public static Customer instanceOf() {
        return new Customer();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public CustomerStatus getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(CustomerStatus customerStatus) {
        this.customerStatus = customerStatus;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

}
