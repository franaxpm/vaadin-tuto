package com.pany.service;

import com.pany.domain.Customer;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author fran
 */
public class CustomerServiceImpl implements CRUDService<Customer> {

    private static CustomerServiceImpl INSTANCE = new CustomerServiceImpl();
    private final List<Customer> customers;
    private int lastId = 0;


    private CustomerServiceImpl() {
        customers = new ArrayList<>();
        try {

            URL systemResource = this.getClass().getClassLoader().getResource("/customers.csv");

            customers.addAll(Files.readAllLines(Paths.get(systemResource.toURI())).stream()
                    .map(s -> s.split(","))
                    .map(Customer::instanceOf)
                    .collect(Collectors.toList()));

            lastId = customers.stream().mapToInt(Customer::getId).max().orElse(0);

        } catch (NullPointerException | URISyntaxException | IOException ignored) {
            System.err.println(Arrays.toString(ignored.getStackTrace()));
        }
    }

    public static synchronized CRUDService<Customer> instance() {
        if (INSTANCE == null) {
            INSTANCE = new CustomerServiceImpl();
        }
        return INSTANCE;
    }

    @Override
    public Customer save(Customer entity) {
        //Checks if the item exists
        if (entity.getId() == Integer.MIN_VALUE) {
            entity.setId(++lastId);
            customers.add(entity);
        }
        return entity;
    }

    @Override
    public Customer delete(Customer entity) {
        customers.remove(entity);
        return entity;

    }

    @Override
    public List<Customer> findByName(String value) {
        return findAll()
                .stream()
                .filter(customer -> customer.getName().toLowerCase().startsWith(value.toLowerCase()))
                .collect(Collectors.toList());
    }

    @Override
    public List<Customer> findAll() {
        return customers;
    }
}
