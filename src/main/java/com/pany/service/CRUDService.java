package com.pany.service;

import java.util.List;

/**
 * @author fran
 */
public interface CRUDService<T> {

    T save(T entity);

    T delete(T entity);

    List<T> findByName(String value);

    List<T> findAll();
}
