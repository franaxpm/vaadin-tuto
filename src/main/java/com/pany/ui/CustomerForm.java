package com.pany.ui;

import com.pany.domain.Customer;
import com.pany.domain.CustomerStatus;
import com.vaadin.data.Validatable;
import com.vaadin.data.Validator;
import com.vaadin.ui.*;

import java.util.Iterator;
import java.util.Optional;

/**
 * @author fran
 */
public class CustomerForm extends FormLayout {

    private final TextField name;
    private final TextField email;
    private final TextField lastName;
    private final ComboBox customerStatus;
    private final DateField birthDate;

    private final Button save;
    private final Button delete;
    private final Button cancel;
    private CustomerGrid grid;

    private Optional<Customer> customer = Optional.empty();


    private CustomerForm() {
        //Setup
        name = new TextField("Name");
        email = new TextField("E-mail");
        lastName = new TextField("Last Name");
        customerStatus = new ComboBox("Customer Status", CustomerStatus.statuses());
        birthDate = new DateField("Birth Date");
        save = new Button("Save");
        delete = new Button("Delete");
        cancel = new Button("Cancel");


        //Validators
        name.setRequired(true);
        lastName.setRequired(true);
        email.setRequired(true);
        customerStatus.setRequired(true);
        birthDate.setRequired(true);
        customerStatus.setNullSelectionAllowed(false);

        name.setImmediate(true);
        email.setImmediate(true);
        customerStatus.setImmediate(true);
        lastName.setImmediate(true);
        //AddComponents
        addComponents(name,
                lastName,
                email,
                customerStatus,
                birthDate,
                new HorizontalLayout(save, delete, cancel));
    }

    public static CustomerForm instanceOf() {
        return new CustomerForm();
    }

    public void init() {
        //listeners

        save.addClickListener((Button.ClickEvent event) -> {
            boolean isValid = true;
            for (Iterator<Component> it = iterator(); it.hasNext() && isValid; ) {
                Component c = it.next();
                if ((c instanceof Validatable)) {
                    AbstractField v = (AbstractField) c;
                    try {
                        v.validate();
                    } catch (Validator.InvalidValueException e) {
                        v.setValidationVisible(true);
                        Notification.show("Please set a value for " + v.getCaption(), Notification.Type.ERROR_MESSAGE);
                        isValid = false;
                    }
                }
            }

            if (isValid) {
                grid.save(fillCustomer(customer.get()));
                reset();
            }
        });

        delete.addClickListener((Button.ClickEvent event) -> {
            grid.delete(customer.get());
            reset();
        });

        cancel.addClickListener(event -> reset());
    }

    private Customer fillCustomer(Customer c) {
        c.setName(name.getValue());
        c.setEmail(email.getValue());
        c.setLastName(lastName.getValue());
        c.setCustomerStatus(CustomerStatus.getCustomerStatusMap().get(customerStatus.getValue()));
        c.setBirthDate(birthDate.getValue());
        return c;
    }

    private void reset() {
        this.setVisible(false);
        this.customer = Optional.empty();

    }

    void edit(Customer customer) {
        this.setVisible(true);
        delete.setEnabled(!(customer.getId() == Integer.MIN_VALUE));
        name.setValue(customer.getName());
        email.setValue(customer.getEmail());
        lastName.setValue(customer.getLastName());
        birthDate.setValue(customer.getBirthDate());
        customerStatus.setValue(customer.getCustomerStatus().getCaption());
        this.customer = Optional.of(customer);
    }

    boolean isEditable() {
        boolean isPresent = !customer.isPresent();
        if (!isPresent) {
            Notification.show("Please finish the current editing", Notification.Type.WARNING_MESSAGE);

        }
        return !customer.isPresent();
    }

    public void setGrid(CustomerGrid grid) {
        this.grid = grid;
    }
}
