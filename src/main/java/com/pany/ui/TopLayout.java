package com.pany.ui;

import com.pany.domain.Customer;
import com.pany.service.CRUDService;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TextField;

/**
 * @author fran
 */
public class TopLayout extends HorizontalLayout {

    private final TextField filter;
    private final Button clean;
    private final Button addNew;
    private final CustomerGrid grid;


    TopLayout(CustomerGrid grid, CustomerForm customerForm, CRUDService<Customer> customerService) {
        this.setSpacing(true);
        this.grid = grid;

        //setup components
        filter = new TextField();
        filter.setInputPrompt("filter by name...");
        clean = new Button("X");
        addNew = new Button("Add new customer");
        //addComponents
        addComponents(new HorizontalLayout(filter, clean), addNew);

        //listeners
        filter.addTextChangeListener(textChangeEvent -> {
            String text = textChangeEvent.getText();
            if ("".equals(text)) {
                resetFilter();
            } else {
                grid.setContainerDataSource(new BeanItemContainer<>(Customer.class, customerService.findByName(text)));
            }
        });
        clean.addClickListener(event -> resetFilter());
        addNew.addClickListener(event -> {
            if (customerForm.isEditable()) {
                customerForm.edit(Customer.instanceOf());
            }
        });
    }

    public static TopLayout instanceOf(CustomerGrid grid, CustomerForm customerForm, CRUDService<Customer> customerService) {
        return new TopLayout(grid, customerForm, customerService);
    }

    private void resetFilter() {
        filter.clear();
        grid.resetData();
    }

}
