package com.pany.ui;

import com.pany.domain.Customer;
import com.pany.service.CRUDService;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.Grid;

import java.util.List;
import java.util.Set;

/**
 * @author fran
 */
public class CustomerGrid extends Grid {

    private final CRUDService<Customer> service;

    private CustomerGrid(CRUDService<Customer> service, CustomerForm customerForm) {
        this.service = service;

        setSelectionMode(SelectionMode.SINGLE);
        resetData();
        this.setColumns("name", "lastName", "email");


        addSelectionListener(event -> {
            Set<Object> selected = event.getSelected();
            if (!selected.isEmpty() && customerForm.isEditable()) {
                Customer customer = (Customer) selected.iterator().next();
                customerForm.edit(customer);
            }
        });


    }

    public static CustomerGrid instanceOf(CRUDService<Customer> service, CustomerForm customerForm) {
        return new CustomerGrid(service, customerForm);
    }

    public void resetData() {
        List<Customer> entities = service.findAll();
        BeanItemContainer<Customer> container = new BeanItemContainer<>(Customer.class, entities);
        this.setContainerDataSource(container);
    }

    public void save(Customer customer) {
        service.save(customer);
        this.resetData();
    }

    public void delete(Customer customer) {
        service.delete(customer);
        this.resetData();
    }
}
